#! /usr/bin/env python3
#
# DESCRIPTION
# =============
# This tool is used to parse a swagger file to:
#   - output the list of endpoints like
#       GET     /api/v1/endpoint
#       POST    /api/v1/endpoint_post
#       GET     /api/v1/another/endpoint
# 
#   - Create a `.sh` file containing raw `curl` commands generated.
#
# Then a simple `bash <output_file>.sh` could be run to query all endpoints 
# within the swagger file
#
# EXAMPLE
# =============
# 
# ```bash
#   # 1. Create the commands
#   $ python SwaggerParser.py -f swagger.json -c <output_file> -x \
# 'http://127.0.0.1:8080' -t <auth_token> 
#   # 2. Send the commands
#   $ bash <swagger_file_generated>.sh
# ```
# 
# USAGE
# =============
#
# ```bash
#   # Print help
#   $ python SwaggerParser.py -h
#
#   # Ouptut the list of endpoints and the matching HTTP verb
#   $ python SwaggerParser.py -l -f swagger.json
#
#   # Output `curl` commands related to each endpoints found and take into 
#   # account params (proxy/auth_token) given in URL in the generated `curl` 
#   # commands
#   $ python SwaggerParser.py -f swagger.json -c <output_file> -x \
# 'http://127.0.0.1:8080' -t <auth_token> 
#
#   # Some swagger file don't have the "hostname" set in the file, therefore it 
#   # can be manually provided through the -H option to be taken into account 
#   # or simply overwrite the existing URL in the Swagger definition
#   $ python SwaggerParser.py -H "http://example.com" -f swagger.json -c \
# <output_file> -x 'http://127.0.0.1:8080' -t <auth_token> 
# ```
#
# INSTALLATION
# =============
#
# ```bash
# pip install rich requests 
# ```
#
# TODO
# ==========
#
#   - [x] Properly parse the "Object" type in `$ref` during creation of POST/PUT
#   request
#   - [x] Add placeholder `content_type` support (e.g. provide a XML sample 
#   file for `application/xml`)
#   - [ ] Also add headers support when the "in":"header" value is found in the 
#   swagger file (currently only dynamic parameters in the query string or the 
#   request body parameters is supported)
#   - [ ] Add `examples` definition support (e.g. when encountering the "Object 
#   type")

#   - [ ] Provide the ability to send the requests from the script and output
#   the responses
#   - [ ] ...
#
# BUG
# ==========
#
# - [x] the `_construct_post_query` method is awful, and the way parameters are
#   parsed is also disgusting within the `_parse_endpoint` function thus prone
#   to multiple error for some edge cases resulting in the following traceback:
#   ```
#   [...] in _construct_post_query
#   post_params.update({
#   TypeError: unhashable type: 'dict'`
#   ```
#   They will be revamped but those functions were written at 4am, so sorry not 
#   sorry.
#
#
# onemask

import json
import sys
import re
import requests 
import logging
import argparse
import os
import datetime

from rich.console import Console
from rich.logging import RichHandler

console = Console()

NL = "\n"
PLACEHOLDER = "XXYYZZ"

# RESTful methods
METHODS = [
    'get',
    'post',
    'put',
    'delete',
    'patch',
]

class Logger():
    def __init__(self, level="INFO"):
        # FORMAT = f"%(name)s\t%(message)s"
        FORMAT = f"%(message)s"
        logging.basicConfig(
            level=level, 
            format=FORMAT, 
            datefmt="[%X]", 
            handlers=[RichHandler()]        
        )

    def get_logger(self, name):
        return logging.getLogger(name.upper())

class SwaggerParser():

    def __init__(self, args):
        self.now = datetime.datetime.now().strftime('Y%m%d-%H%M%S%f')
        # self.term_nc_column  = os.get_terminal_size().columns
        self.term_nc_column  = 40
        self.data            = None
        self.version         = None
        self.api_description = None
        self.output_dir      = None
        self.hostname        = None
        self.api_base_path   = None
        self.api_path        = None
        self.token           = None
        self.schemes         = []
        self.endpoints       = []
        self.cmd             = []
        self.args            = args

        if args.verbose:
            self.logger = Logger(level="DEBUG").get_logger("swagger-parser")
        else:
            self.logger = Logger().get_logger("swagger-parser")

        self.hostname     = args.hostname.rstrip('/') if args.hostname else None
        self.proxy        = {"all": args.proxy if args.proxy else None}

        try:
            self.logger.debug(args.file)
            with open(args.file, 'r') as f:
                data = json.load(f)
            self.data = data

        except ValueError:
            self.logger.error("JSON file is corrupt!")
            sys.exit(-1)

        except Exception as e:
            self.logger.error("Error while opening the file")
            sys.exit(-1)

        if "title" in self.data['info']:
            desc = self.data['info']['title']
            output_name = f"swagger_{desc.replace(' ','_')}.sh"
        else:
            output_name = f"swagger_{self.now}.sh"
        self.command_file = args.command if args.command else output_name

        self._init_http_client()
        self._parse_swagger()
        
    def _init_http_client(self):
        self.ua = "" 
        self.ua += "Mozilla/5.0 (Windows NT 10.0; Win64; x64) "
        self.ua += "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.150"

        headers = {
            "User-Agent": self.ua
        }

        self.session = requests.Session()
        self.session.proxies.update(self.proxy)
        self.session.verify = False
        self.session.headers.update(headers)
        if self.args.token : 
            self.session.headers.update({
                "Authorization": f"Bearer {self.args.token}"
                })

    def _parse_swagger(self):

        if "swagger" not in self.data and "openapi" not in self.data:
            self.logger.debug(self.data.keys())
            self.logger.error(
                f"Incorrect file type? No swagger or openapi definition found"
                )
            raise SystemError()

        if "swagger" in self.data:
            self._version = self.data["swagger"]
            if not self.hostname:
                self.hostname = self.data['host']
            self.logger.debug(
                f'Swagger file version {self._version} identified.'
                )

        else:
            self._version = self.data["openapi"]
            if not self.hostname:
                try:
                    self.hostname = self.data['servers'][0]["url"]
                except:
                    self.hostname = "https://<REPLACEME>/"
            
            self.logger.debug(
                f'Openapi file version {self._version} identified.'
                )

        self.api_paths       = self.data['paths']
        if "description" in self.data["info"]:
            self.api_description = self.data['info']['description']
        else:
            self.api_description = "No API description in swagger file."

        if "basePath" in self.data:
                self.api_base_path   = self.data['basePath']
                self.hostname += self.api_base_path

        self.schemes.append(
            self.data["schemes"][0] if "schemes" in self.data else "https"
            )

        self.logger.info(f'Target URL: {self.hostname}')
        self.logger.info(f"API basepath: {self.api_base_path}")
        self.logger.info(f'API description: {self.api_description}')
        self.logger.info(f"Found {len(self.data['paths'])} endpoints")

        if self.args.list_endpoints:
            self._output_endpoints()
            return
        
        for path in self.data['paths'].keys():
            self._parse_endpoint(path)

        with open(self.command_file, 'w') as f:
            self.logger.info(f"Commands ({len(self.cmd)}):")
            print()
            print("-"*self.term_nc_column)
            for cmd in self.cmd:
                f.write(cmd)
                console.print(cmd)
                f.write('\n')
            print("-"*self.term_nc_column)
            print()

        self.logger.info(
            f"{len(self.cmd)} commands saved to '{self.command_file}'"
            )

    def _output_endpoints(self):
        for path in self.data['paths'].keys():
            for m in METHODS:
                if m in self.data['paths'][path]:
                    self.logger.info(f'{m.upper()}\t{path}')

    def _parse_endpoint(self, endpoint):

        self.logger.debug(f"Parsing {endpoint}")
        data = self.data
        # For each methods
        for m in METHODS:
            params = []
            content_type = None
            
            # Search for each methods on each endpoints
            if m in self.data["paths"][endpoint]:
                cmd_added = False
                m_data = self.data["paths"][endpoint][m]
                # Check if methods has parameters
                # 1. Parameters are within the URL path (e.g. /v1/a/{c}?a={b})
                if "parameters" in m_data:
                    params_schema = m_data['parameters']
                    self.logger.debug(
                        f'[_parse_endpoint] {m.upper()} {endpoint}:\n'
                        f'Schema: {params_schema}')
                    for p in params_schema:
                        # We create a new_param object
                        new_param = {
                            'name': None,
                            'in'  : None,
                            'type': ''
                        }
                        # We iterate through each value of the p param schema
                        # to find its 'name' and 'in' values
                        for k,v in p.items():
                            if k == "$ref":
                                params += self._get_params_from_model(v)
                            elif k == "name":
                                new_param['name'] = v
                            elif k == "in":
                                new_param['in'] = v
                            else:
                                pass
                                # self.logger.debug(
                                #  f'Ignoring key/value: "{k}":"{v}" for "{p}"')
                        
                        # When a new param is found and he's got it's name and 
                        # "in" values set, we add it to our params variable
                        if new_param["name"] is not None \
                            and new_param["in"] is not None:
                            
                            params.append(new_param)

                        self.logger.debug(f"Adding new param: {new_param}")

                # 2. Params are within the request body
                if "requestBody" in m_data:
                    if "content" in m_data['requestBody']:
                        content = m_data['requestBody']["content"]
                        for v in list(content.keys()):
                            content_type = v
                            self.logger.debug(content[v])
                            current_schema = content[content_type]["schema"]
                            if "properties" in current_schema:
                                request_params = current_schema["properties"]
                                for p in request_params:
                                    params.append({
                                        'name':p,
                                        'in': "requestBody"
                                    })
                                self.logger.debug(current_schema)
                            
                            ####################################################
                            ####################################################
                            ####################################################

                            # 3. Params are referenced elsewhere
                            if "$ref" in current_schema:
                                ref = current_schema["$ref"]
                                # We fetch the param definition from their 
                                # reference
                                params = self._get_params_from_model(ref)

                            self._create_curl_command(
                                endpoint,
                                method=m.upper(),
                                params=params,
                                content_type=content_type
                            )

                            cmd_added = True

                if not cmd_added:
                    self._create_curl_command(
                        endpoint,
                        method=m.upper(),
                        params=params,
                        content_type=content_type
                    )

    def _get_params_from_model(self, ref):
        self.logger.debug(f"Searching for reference {ref}")
        params    = []
        ref_parts = ref.split('/')[1:]
        target    = self.data

        for part in ref_parts: 
            target = target[part]
        
        obj_name = ref.split('/')[-1]
        # self.logger.debug(target)
        
        if "type" in target:
            if "schema" in target["type"]:
                if target["schema"]["type"] == "object":
                    properties = target["properties"]
                    obj_params = []
                    # self.logger.debug(f"{properties}")
                    new_param = {
                        'name': None,
                        'type': None,
                        'in': None
                    }
                    for k, v in properties.items():
                        if k == "type":
                            new_param['type'] = v
                        elif k == "name":
                            new_param['name'] = v
                        elif k == "in":
                            new_param['in'] = v
                    params.append(new_param)
                else:
                    # self.logger.warning("WHAT TO DO HERE?")
                    pass
            else:
                if "properties" in target:
                    properties = target["properties"]
                    obj_params = {
                        obj_name :[]
                    }

                    for k, v in properties.items():
                        new_param = {
                            'name': None,
                            'type': None,
                            'in'  : None
                        }
                        new_param["name"] = k
                        if "type" in v:
                            new_param["type"] = v["type"]
                        elif "in" in v:
                            new_param["in"] = v["in"]
                        else:
                            pass
                        
                        # Add the new objet to our params list "obj_params"
                        if new_param["name"] is not None and \
                            new_param["type"] is not None:
                            if new_param["in"] is None:
                                new_param["in"] = "requestBody"

                            obj_params[obj_name].append(new_param)

                    self.logger.debug("REF: parsing object?")
                    self.logger.debug(f'{ref}: obj_params: {obj_params}')
                    params.append({
                        'name': obj_params,
                        'type': "object",
                        'in':'requestBody'
                        })
                    self.logger.debug(f'{ref}: Final parsed params: {params}')
                    return params

                else:
                    self.logger.debug(target)
                    params.append({
                        'name':obj_name,
                        'in':'requestBody'
                    })


        elif "in" in target: 
            self.logger.debug(f"No type defined for reference: {target}")
            params.append({
                'name': target["name"],
                'in': target["in"],
                'type': target["schema"]["type"]
            })
        
        # We default to "requestBody" as where the parameter should go if not
        # indicated
        else: 
            params.append({
                'name': target["name"],
                'in': 'requestBody',
                'type': target["schema"]["type"]
            })
        
        BS="\\"
        self.logger.debug(f'{ref}: Final parsed params: {params}')
        return params

    def _create_curl_command(
        self, 
        endpoint, 
        method=None, 
        params=None, 
        content_type=None
        ):
        self.logger.debug(
            f"Generating curl command for endpoint {method} {endpoint} "
            f"with params -> {params}"
            )
        hostname     = self.hostname
        content_type = content_type if content_type is not None else None
        cmd          = f'curl -sk -o /dev/null -H "User-Agent: {self.ua}" '

        for scheme in self.schemes:
            if self.hostname.startswith('https://') or \
                self.hostname.startswith('http://'):

                hostname = "/".join(self.hostname.split('/')[2:])

            url = scheme + "://" + hostname + endpoint

            if self.proxy['all'] != None:
                cmd += f"-x {self.proxy['all']} "

            # Construct query string here
            getquery = self._construct_get_query(params, url, endpoint)

            # If it's a GET or DELETE request:
            if method == "GET" or method == "DELETE":
                cmd += f'-X {method} '
                if method == "DELETE":
                    cmd = "# " + cmd 
            
            # If it's a POST, PUT, PATCH request:
            else:
                cmd += f"-X {method} " 
                if content_type is not None:
                    cmd += f'-H "Content-Type: {content_type}" '
                    if len(params) > 0:
                        post_params = self._construct_post_query(params)
                        if len(post_params) > 0:
                            # Check Content-Types here:
                                if "json" in content_type:
                                    cmd += f"-d '{json.dumps(post_params)}' "
                                elif "xml" in content_type:
                                    cmd += f"-d \"{self._gen_dummy_xml(params)}\" "
                                    pass
                                elif "form-data" in content_type:
                                    self.logger.debug("[CURL] FORM-DATA SPOTTED")
                                    self.logger.debug(post_params)
                                    for k, v in post_params.items():
                                        cmd += f"-F {k}={v} "
                                    pass
                                elif "urlencoded" in content_type:
                                    form_data = ""
                                    for k, v in post_params.items():
                                        form_data += f'{k}={v}&'
                                    cmd += f"-d \'{(form_data.rstrip('&'))}\' "
                                else:
                                    cmd += f"-d '{json.dumps(post_params)}' "
                else:
                    self.logger.warning(
                        f"No content type for {endpoint}"
                        )

            cmd += f'\'{getquery}\' '
        
            if self.args.token:
                cmd += f'-H "Authorization: Bearer {self.args.token}" '

            cmd += "& "

        self.logger.debug(f"CMD: {cmd}")
        self.cmd.append(cmd)


    def _gen_dummy_xml(self, params):
        xml_file = ''
        xml_file += '<?xml version="1.0" encoding="UTF-8"?>'
        xml_file += '<note><style id="autoconsent-prehide"/><to>Tove</to>'
        xml_file += '<from>Jani</from><heading>Reminder</heading><body>'
        xml_file += 'Don\'t forget me this weekend!</body></note>'

        return xml_file
    
   
    def _construct_get_query(self, params, url, endpoint):
        # self.logger.debug(f"in construct_get_query({params},{url},{endpoint}")
        params        = params
        querystring   = ""
        query_arg_cpt = 0

        if endpoint in url:
            querystring = url
        else:
            querystring = url + endpoint

        if len(params) > 0:
            for p in params:
                if p["in"] == "path":
                    p_name = p['name']
                    querystring = re.sub(f"{p_name}", PLACEHOLDER, querystring)
                elif p["in"] == "query":
                    if query_arg_cpt == 0:
                        querystring += f'?{p["name"]}={PLACEHOLDER}'
                    else:
                        querystring += f'&{p["name"]}={PLACEHOLDER}'
                    query_arg_cpt += 1
                else:
                    pass
        else:
            querystring = url

        self.logger.debug(f"[_construct_query_string] {querystring}")
        return querystring
    
    def _construct_post_query(self, params):
        self.logger.debug(f"in post query: {params}")
        post_params = {}
        
        for p in params:
            if p['in'] == "requestBody":
                if "type" in p:
                    self.logger.debug(f"[_construct_post_query] parsing {p}")
                    # If we parse an JSON object
                    if p["type"] == "object":
                        # Add the key-value pairs from the sub_p 
                        # dictionaries to the sub_obj dictionary
                        current_objet = [next(iter(p["name"].keys()))][0]
                        sub_obj = {}
                        for item in p["name"][current_objet]:
                            sub_obj.update({
                                item['name']: PLACEHOLDER,
                                })

                        self.logger.debug(f'subobj: {sub_obj}')
                        if len(sub_obj) > 0:
                            # If we want the JSON model name in the output we
                            # can uncomment the next line and comment the next 
                            # one after it

                            # post_params[current_objet] = sub_obj
                            post_params.update(sub_obj)

                    else:
                        self.logger.warning(
                            f"{p} type is not object, what to do?"
                            )
                        pass
                else:
                    # self.logger.debug(f'_construct_post_query: {p}')
                    if isinstance(p["name"], list):
                        for sub_p in p["name"]:
                            post_params.update({
                                sub_p["name"]: PLACEHOLDER
                            })
                    else:
                        self.logger.debug(p)
                        post_params.update({
                            p["name"]: PLACEHOLDER
                        })
            elif p["in"] == "header":
                self.logger.warning(f"HEADER {p['name']} NOT ADDED !")
            else:
                post_params.update({
                    p["name"]: PLACEHOLDER
                })
    
        return post_params

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-f', 
        '--file',
        help='Swagger JSON file',
        required=True
        )
    parser.add_argument(
        '-x', 
        '--proxy',
        help='Proxy URL'
        )
    parser.add_argument(
        '-t',
        '--token',
        help="Authentication token"
    )
    parser.add_argument(
        '-c',
        '--command',
        help="File to output curl commands to"
    )
    parser.add_argument(
        '-v',
        '--verbose',
        action="store_true",
        help="Enable verbosity"
    )
    parser.add_argument(
        '-H',
        '--hostname',
        help="""
        Specify the hostname manually (e.g. if not found in swagger or if 
        different from the swagger file)
        """
    )
    parser.add_argument(
        '-l',
        '--list-endpoints',
        action="store_true",
        help="Only list the endpoints in the swagger file"
    )

    args = parser.parse_args()

    return args

def main():
    args = parse_args()
    x = SwaggerParser(args)

if __name__ == '__main__':
    main()