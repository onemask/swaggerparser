# SwaggerParser

## DESCRIPTION

This tool is used to parse a swagger file to:

- 1. output the list of endpoints like
```
GET     /api/v1/endpoint
POST    /api/v1/endpoint_post
GET     /api/v1/another/endpoint
...
```

- 2. Create a `.sh` file containing raw `curl` commands generated.

Then a simple `bash <output_file>.sh` could be run to query all endpoints
within the swagger file

## EXAMPLE

```bash
# 1. Create the commands
$ python SwaggerParser.py -f swagger.json -c <output_file> -x \
'http://127.0.0.1:8080' -t <auth_token>
# 2. Send the commands
$ bash <swagger_file_generated>.sh
```

Other examples:

**1. List all endpoints**

![Example 1](./img/swagger_img2.png)

**2. Generate `curl` commands**

![Example 1](./img/swagger_img1.png)


## USAGE

```bash
# Print help
$ python SwaggerParser.py -h

# Ouptut the list of endpoints and the matching HTTP verb
$ python SwaggerParser.py -l -f swagger.json

# Output `curl` commands related to each endpoints found and take into
# account params (proxy/auth_token) given in URL in the generated `curl`
# commands
$ python SwaggerParser.py -f swagger.json -c <output_file> -x \
'http://127.0.0.1:8080' -t <auth_token>

# Some swagger file don't have the "hostname" set in the file, therefore it
# can be manually provided through the -H option to be taken into account
# or simply overwrite the existing URL in the Swagger definition
$ python SwaggerParser.py -H "http://example.com" -f swagger.json -c \
<output_file> -x 'http://127.0.0.1:8080' -t <auth_token>
```

## HELP

```console
$ python3 SwaggerParser.py -h
usage: SwaggerParser.py [-h] -f FILE [-x PROXY] [-t TOKEN] [-c COMMAND] [-v] [-H HOSTNAME] [-l]

options:
  -h, --help            show this help message and exit
  -f FILE, --file FILE  Swagger JSON file
  -x PROXY, --proxy PROXY
                        Proxy URL
  -t TOKEN, --token TOKEN
                        Authentication token
  -c COMMAND, --command COMMAND
                        File to output curl commands to
  -v, --verbose         Enable verbosity
  -H HOSTNAME, --hostname HOSTNAME
                        Specify the hostname manually (e.g. if not found in swagger or if different from the swagger file)
  -l, --list-endpoints  Only list the endpoints in the swagger file
```

## INSTALLATION

```bash
git clone https://gitlab.com/onemask/swaggerparser
cd swaggerparser
pip install rich requests # requests is useless for now though

# (optional) Install it globally
sudo ln -s $(pwd)/SwaggerParser.py /usr/local/bin/swaggerparser.py
sudo chmod +x !$
# Then can be run from anywhere using `swaggerparser.py`
```

## TODO

- [ ] Add headers support when the `"in":"header"` value is found in the
swagger file (currently only dynamic parameters in the query string or the
request body parameters is supported)
- [ ] Add `examples` definition support when it's in the swagger file
- [ ] Provide the ability to send the requests from the script and output
the responses

## Disclaimer

Some functions are hideous so don't force yourself to look at the code as it may
induce severe trauma. But hey, they were written at 4 a.m. so indulge me! I'll
try to update them later. Or maybe not. Most likely not.


## Author

- `onemask`
